package cn.ant.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 远程调用
 */
@FeignClient("auth")//value表示的是要从那个微服务远程调用相关接口
@Component
public interface AuthRemoteService {
    /**
     * 获取Token
     * @return
     */
    @GetMapping(value = "/remote/auth/getToken")
    public String getToken();
}
