package cn.ant.annotation;

import java.lang.annotation.*;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/7
 * @Content:
 */
//type表示可以放在类  method表示可以放在方法
@Target({ElementType.TYPE,ElementType.METHOD})
//什么时期表执行
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AopTimerLog {
}
