//package cn.ant.config;
//
//import lombok.Data;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
////@Configuration
////@ConfigurationProperties(prefix = "redisson")
////@ConditionalOnProperty("redission.password")
////@Data
//public class RedissonConfig {
//
//    private String address;
//
//    private String password;
//
//    private Integer timeout=3000;
//
//    private Integer database=0;
//
//    private Integer connectionPoolMaxSize=64;
//
//    private Integer connectionPoolMinIdleSize=10;
//
//    private Integer slaveConnectionPoolSize=250;
//
//    private Integer masterConnectionPoolSize=250;
//
//    private String[] sentineAddress;
//
//    private String masterName;
//
//
//}
