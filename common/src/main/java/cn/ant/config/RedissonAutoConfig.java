//package cn.ant.config;
//
//import cn.hutool.core.util.StrUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.redisson.Redisson;
//import org.redisson.api.RedissonClient;
//import org.redisson.config.Config;
//import org.redisson.config.SingleServerConfig;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
////@Configuration
////@ConditionalOnClass(Config.class)
//@Slf4j
//public class RedissonAutoConfig {
//
//    @Autowired
//    private RedissonConfig redissonConfig;
//    /**
//     * 单机模式自动装配
//     * @return
//     */
//    @Bean
//    public RedissonClient redissonSingleClientConfig(){
//        log.info("redissonSingleClientConfig.start");
//        String address = redissonConfig.getAddress();
//        log.info("redission address is {}", address);
//        String password = redissonConfig.getPassword();
//        log.info("redission password is {}", password);
//        Integer timeout = redissonConfig.getTimeout();
//        log.info("redission timeout is {}", timeout);
//        Integer connectionPoolMaxSize = redissonConfig.getConnectionPoolMaxSize();
//        log.info("redission connectionPoolMaxSize is {}", connectionPoolMaxSize);
//        Integer connectionPoolMinIdleSize = redissonConfig.getConnectionPoolMinIdleSize();
//        log.info("redission connectionPoolMinIdleSize is {}", connectionPoolMinIdleSize);
//        Config config = new Config();
//        SingleServerConfig serverConfig = config.useSingleServer();
//        serverConfig.setAddress(address);
//        serverConfig.setTimeout(timeout);
//        serverConfig.setConnectionPoolSize(connectionPoolMaxSize);
//        serverConfig.setConnectionMinimumIdleSize(connectionPoolMinIdleSize);
//        if (StrUtil.isNotBlank(password)){
//            serverConfig.setPassword(password);
//        }
//
//        return Redisson.create(config);
//    }
//}
