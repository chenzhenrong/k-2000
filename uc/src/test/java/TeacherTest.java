import cn.ant.UcApplication;
import cn.ant.entity.Teacher;
import cn.ant.mapper.TeacherMapper;
import cn.ant.service.UserManagerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/11
 * @Content:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UcApplication.class)
@Slf4j
public class TeacherTest {

    @Autowired
    private UserManagerService userManagerService;

    @Autowired
    private TeacherMapper teacherMapper;

    @Test
    public void test(){
        Page<Teacher> teacherPage = new Page<>();
        teacherPage.setCurrent(1);
        teacherPage.setSize(10);
        IPage<Teacher> page = userManagerService.page(teacherPage, new QueryWrapper<Teacher>().lambda()
                .like(Teacher::getTName, "张")
        );
        log.info("page ==>{}",page.getRecords());
    }
}
