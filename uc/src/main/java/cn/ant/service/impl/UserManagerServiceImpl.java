package cn.ant.service.impl;

import cn.ant.entity.Teacher;
import cn.ant.mapper.TeacherMapper;
import cn.ant.remote.AuthRemoteService;
import cn.ant.service.UserManagerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserManagerServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements UserManagerService {

//    @Autowired
//    private AuthRemoteService authRemoteService;
//
//    @Override
//    public String getUserToken() {
//        String token = authRemoteService.getToken();
//        log.info("getUserToken is {}",token);
//        return token;
//    }
}
