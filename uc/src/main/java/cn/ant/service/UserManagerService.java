package cn.ant.service;

import cn.ant.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

@Service
public interface UserManagerService extends IService<Teacher> {
//    public String getUserToken();
}
