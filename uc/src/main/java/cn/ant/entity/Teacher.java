package cn.ant.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/11
 * @Content:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {
    private String tId;
    private String tName;
}
