package cn.ant.schedule;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/12
 * @Content:
 */
@Component
@Slf4j
public class XXLJobSimpleSampleSchedule {
    @XxlJob(value = "XXLJobSimpleSampleSchedule")
    public ReturnT<String> test(String param){
        log.info("XXLJobSimpleSampleSchedule.start");
        log.info("param is {} time is {}ms",param,System.currentTimeMillis());
        log.info("XXLJobSimpleSampleSchedule.start");
        return ReturnT.SUCCESS;
    }
}
