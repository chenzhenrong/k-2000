package cn.ant;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@EnableFeignClients({"cn.ant"})
@Slf4j
@MapperScan("cn.ant.mapper")
public class UcApplication {
    public static void main(String[] args) {
        log.info("UcApplication ==>start");
        SpringApplication.run(UcApplication.class,args);
        log.info("UcApplication ==>end");
    }
}
