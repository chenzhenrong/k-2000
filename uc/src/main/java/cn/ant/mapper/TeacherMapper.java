package cn.ant.mapper;

import cn.ant.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/11
 * @Content:
 */
@Repository
public interface TeacherMapper extends BaseMapper<Teacher> {
}
