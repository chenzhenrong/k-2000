package cn.ant.service;

import org.flowable.task.api.Task;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public interface FlowTransactionalService {
    public  void completeTask(String taskId);
}
