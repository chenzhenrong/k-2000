package cn.ant.service;

import cn.ant.entity.FlowRollbackParams;
import cn.ant.entity.FlowStartParams;
import cn.hutool.json.JSON;
import org.flowable.task.api.Task;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FlowManagerService {
    /**
     * 部署流程
     * @param flowFileName
     * @return
     */
    public String deployFlow(String flowFileName);

    /**
     * 启动流程
     * @param flowStartParams
     * @return
     */
    public String startFlow(FlowStartParams flowStartParams);

    /**
     * 通过任务Id完成任务
     * @param taskId
     * @return
     */
    public String completeTaskByTaskId(String taskId);

    /**
     * 回滚或跳过向前（不存在子流程）
     * @param flowRollbackParams
     */
    public void rollBackSingle(FlowRollbackParams flowRollbackParams);

    /**
     * 获取正在执行的任务列表信息
     * @return
     */
    public List<Task> getRunningTaskList();

    /**
     * 批量处理 通过流程id
     * @param processIds
     */
    public void batchTaskByFlowId(List<String> processIds);

    /**
     * 批量处理异步作业
     * @param processIds
     */
    public void batchJobByFlowId(List<String> processIds);


    /**
     * 判断流程是否已经结束
     * @param processInstanceId
     * @return
     */
    public boolean isFinished(String processInstanceId);
    /**
     * 获取流程图
     * @param processInstanceId
     * @return
     */
    public byte[] getFlowImage(String processInstanceId) throws Exception;
}
