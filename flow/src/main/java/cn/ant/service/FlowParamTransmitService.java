package cn.ant.service;

import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/19
 * @Content:
 */
@Service
public interface FlowParamTransmitService {
    /**
     * 带参数流转节点
     * @param taskId
     * @param map
     * @return
     */
    public void completeTaskParam(String taskId, Map<String,Object> map);

    /**
     * 带参数流转节点 设置类型为false(非全局)
     * @param taskId
     * @param map
     * @return
     */
    public String completeTaskParamChooseThree(String taskId, Map<String,Object> map,boolean flag);


    /**
     * 获取当前任务的参数
     * @param taskId
     * @return
     */
    public Map<String, Object> getCompleteTaskParam(String taskId);

    /**
     * 设置任务参数
     * @param taskId
     * @param map
     */
    public void setCompleteTaskParam(String taskId,Map<String,Object> map);
}
