package cn.ant.service.impl;

import cn.ant.service.FlowTransactionalService;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class FlowTransactionalServiceImpl implements FlowTransactionalService {

    @Autowired
    private TaskService taskService;

    @Transactional(rollbackFor = {RuntimeException.class, Exception.class}, propagation = Propagation.NESTED,isolation = Isolation.READ_UNCOMMITTED)
    @Override
    public void completeTask(String taskId) {
        Task task1 = taskService.createTaskQuery().taskId(taskId).singleResult();
        log.info("task1 {}",task1);
        taskService.complete(taskId);
        Task task2 = taskService.createTaskQuery().taskId(taskId).singleResult();
        log.info("task2 {}",task2);
    }
}
