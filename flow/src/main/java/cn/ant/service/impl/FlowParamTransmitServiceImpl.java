package cn.ant.service.impl;

import cn.ant.service.FlowParamTransmitService;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/19
 * @Content:
 */
@Service
@Slf4j
public class FlowParamTransmitServiceImpl implements FlowParamTransmitService {
    @Autowired
    private TaskService taskService;

    @Override
    public void completeTaskParam(String taskId, Map<String, Object> map) {
        log.info("completeTaskParam.start");
        String s = new Date().toLocaleString();
        map.put("time",s);
        taskService.complete(taskId,map);
        log.info("completeTaskParam.end");
    }

    @Override
    public String completeTaskParamChooseThree(String taskId, Map<String, Object> map, boolean flag) {
        log.info("completeTaskParamChooseThree.start");
        String s = new Date().toLocaleString();
        map.put("time",s);
        taskService.complete(taskId,map,flag);
        log.info("completeTaskParamChooseThree.end");
        return null;
    }

    @Override
    public Map<String, Object> getCompleteTaskParam(String taskId) {
        log.info("getCompleteTaskParam.start");
        Map<String, Object> variables = taskService.getVariables(taskId);
        log.info("task variables is {}",variables);
        log.info("getCompleteTaskParam.end");
        return variables;
    }

    @Override
    public void setCompleteTaskParam(String taskId, Map<String, Object> map) {
        log.info("setCompleteTaskParam.start");
        String s = new Date().toLocaleString();
        map.put("time",s);
        taskService.setVariables(taskId,map);
        log.info("setCompleteTaskParam.end");
    }
}
