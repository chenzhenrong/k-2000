package cn.ant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@Slf4j
@EnableTransactionManagement
public class FlowApplication {
    public static void main(String[] args) {
        log.info("=======================>FlowApplication.start");
        SpringApplication.run(FlowApplication.class,args);
        log.info("=======================>FlowApplication.finish");
    }
}
