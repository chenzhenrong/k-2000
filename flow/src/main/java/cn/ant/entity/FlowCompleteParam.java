package cn.ant.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/19
 * @Content:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlowCompleteParam implements Serializable {
    private String taskId;
    private Map<String,Object> map;
    private Boolean flag;
}
