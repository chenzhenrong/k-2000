package cn.ant.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlowStartParams implements Serializable {
    /**
     * 流程定义Id（key）
     */
    private String flowDefinitionId;

    /**
     * 启动流程的参数
     */
    private Map<String,Object> data;
}
