package cn.ant.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlowRollbackParams implements Serializable {
    /**
     * 当前流程实例Id
     */
    private String flowInstanceId;

    /**
     * 当前正在处理的节点Id（key）
     */
    private String flowCurrentTaskDefinitionId;

    /**
     * 要回滚到的节点Id（key）
     */
    private String flowRollbackTaskDefinitionId;
}
