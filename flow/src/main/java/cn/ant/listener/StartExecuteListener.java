package cn.ant.listener;


import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;

@Component(value = "startExecuteListener")
@Slf4j
public class StartExecuteListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) {
        log.info("startExecuteListener.start {}",delegateExecution);
    }
}
