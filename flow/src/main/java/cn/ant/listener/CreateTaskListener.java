package cn.ant.listener;

import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.api.Task;
import org.flowable.task.service.delegate.DelegateTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Component(value = "createTaskListener")
@Slf4j
public class CreateTaskListener implements TaskListener {
    /**
     * 任务初始化的时候，被触发 在这里任务Id开始写入数据表
     * @param delegateTask
     */
    @Autowired
    private TaskService taskService;
    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("CreateTaskListener.start taskId is {}",delegateTask.getId());
        log.info("===========>delegateTask.getVariables {}",delegateTask.getVariables());
        HashMap<String, Object> map = new HashMap<>();
        map.put("time-name-id"+delegateTask.getName(),new Date().toLocaleString()+"=========>"+delegateTask.getName()+"====>"+delegateTask.getId());
        taskService.complete(delegateTask.getId(),map);
        log.info("CreateTaskListener.end taskId is {}",delegateTask.getId());

    }
}
