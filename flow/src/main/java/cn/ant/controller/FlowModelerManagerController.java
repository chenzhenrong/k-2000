package cn.ant.controller;

import org.flowable.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/21
 * @Content:
 */
@RestController
public class FlowModelerManagerController {
    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private ManagementService managementService;

    @Autowired
    private HistoryService historyService;

    public void test(){

    }
}
