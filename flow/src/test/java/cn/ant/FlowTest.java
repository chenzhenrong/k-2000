package cn.ant;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/19
 * @Content:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlowApplication.class)
@Slf4j
public class FlowTest {
    @Autowired
    private TaskService taskService;
    @Autowired
    private RuntimeService runtimeService;

    @Test
    public void test(){
        Object variable = taskService.getVariableInstances("77bed523-30bd-11ec-86f9-646ee0fcfdb9");
        log.info(">>>>>>>>>>>>>variable {}",variable);
    }
}
