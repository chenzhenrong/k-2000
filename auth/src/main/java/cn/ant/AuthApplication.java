package cn.ant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients({"cn.ant"})
@Slf4j
public class AuthApplication {
    public static void main(String[] args) {
        log.info("AuthApplication.start");
        SpringApplication.run(AuthApplication.class,args);
        log.info("AuthApplication.finish");
    }
}
