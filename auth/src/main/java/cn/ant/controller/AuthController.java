package cn.ant.controller;

import cn.hutool.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("auth")
@Slf4j
public class AuthController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping(value = "/getServiceInfoByName/{serviceName}",method = RequestMethod.GET)
    public List<ServiceInstance> getServiceInfoByName(@PathVariable("serviceName") String name){
        List<ServiceInstance> instances = discoveryClient.getInstances(name);
        return instances;
    }
    
    @RequestMapping(value = "/getUserInfoById/{id}",method = RequestMethod.GET)
    public String getUserInfoById(@PathVariable("id")  String id){
        List<ServiceInstance> uc = discoveryClient.getInstances("uc");
        ServiceInstance serviceInstance = uc.get(0);
        log.info("===>{}",serviceInstance);
        URI uri = serviceInstance.getUri();
        StringBuffer url = new StringBuffer();
        url.append(uri.toString());
        url.append("/uc/getUserInfoById/");
        url.append(id);
        log.info("===>{}",url);
        String result = HttpUtil.get(url.toString());
        return result;
    }
    @RequestMapping(value = "/getOpenFeignInfo/{test}")
    public String getOpenFeignInfo(@PathVariable("test") String test){
        return new Date().toLocaleString()+" "+test;
    }
}
