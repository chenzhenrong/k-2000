package cn.ant.remote;

//import cn.ant.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Slf4j
public class AuthRemoteServiceImpl implements AuthRemoteService {

//    @Autowired
//    private RedisUtil redisUtil;

    @Override
    public String getToken() {
        String token = UUID.randomUUID().toString();
        return token;
    }
}
