//package cn.ant.config;
//
//import org.flowable.ui.common.service.idm.RemoteIdmServiceImpl;
//import org.flowable.ui.modeler.properties.FlowableModelerAppProperties;
//import org.flowable.ui.modeler.servlet.ApiDispatcherServletConfiguration;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.boot.web.servlet.ServletRegistrationBean;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.FilterType;
//import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
//import org.springframework.web.servlet.DispatcherServlet;
//
///**
// * @Version 1.0
// * @Author:lss
// * @Date:2021/10/22
// * @Content:
// */
//@Configuration
//@EnableConfigurationProperties(FlowableModelerAppProperties.class)
//@ComponentScan(basePackages = {
//        "org.flowable.ui.common.service",
//        "org.flowable.ui.common.repository",
//        "org.flowable.ui.common.tenant",
//        "org.flowable.ui.modeler.service",
//        "org.flowable.ui.modeler.repository"}, excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RemoteIdmServiceImpl.class)})
//public class FlowableConfig {
//    @Bean
//    public ServletRegistrationBean apiServlet(ApplicationContext applicationContext) {
//        AnnotationConfigWebApplicationContext dispatcherServletConfiguration = new AnnotationConfigWebApplicationContext();
//        dispatcherServletConfiguration.setParent(applicationContext);
//        dispatcherServletConfiguration.register(ApiDispatcherServletConfiguration.class);
//        DispatcherServlet servlet = new DispatcherServlet(dispatcherServletConfiguration);
//        ServletRegistrationBean registrationBean = new ServletRegistrationBean(servlet, "/api/*");
//        registrationBean.setName("Flowable Modeler App API Servlet");
//        registrationBean.setLoadOnStartup(1);
//        registrationBean.setAsyncSupported(true);
//        return registrationBean;
//    }
//}
