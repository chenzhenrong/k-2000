//package cn.ant.controller;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @Version 1.0
// * @Author:lss
// * @Date:2021/10/22
// * @Content:
// */
//@Controller
//@RequestMapping(value = "flowableUserManager")
//@Slf4j
//public class FlowableUserManagerController {
//    @RequestMapping(value = "/rest/account", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public UserRepresentation getAccount() {
//        UserRepresentation userRepresentation = new UserRepresentation();
//        userRepresentation.setId("admin");
//        userRepresentation.setEmail("admin@flowable.org");
//        userRepresentation.setFullName("Test Administrator");
//        userRepresentation.setLastName("Administrator");
//        userRepresentation.setFirstName("Test");
//        List<String> privileges = new ArrayList<>();
//        privileges.add(DefaultPrivileges.ACCESS_MODELER);
//        privileges.add(DefaultPrivileges.ACCESS_IDM);
//        privileges.add(DefaultPrivileges.ACCESS_ADMIN);
//        privileges.add(DefaultPrivileges.ACCESS_TASK);
//        privileges.add(DefaultPrivileges.ACCESS_REST_API);
//        userRepresentation.setPrivileges(privileges);
//        return userRepresentation;
//    }
//
//}
