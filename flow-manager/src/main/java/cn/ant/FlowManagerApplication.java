package cn.ant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Version 1.0
 * @Author:lss
 * @Date:2021/10/22
 * @Content:
 */
@SpringBootApplication
@Slf4j
public class FlowManagerApplication {
    public static void main(String[] args) {
        log.info("=======================>FlowManagerApplication.start");
        SpringApplication.run(FlowManagerApplication.class,args);
        log.info("=======================>FlowManagerApplication.finish");
    }
}
