# k-2000

#### 介绍
k-2000，追求无BUG（狗头保命）

#### 软件架构
无


#### 安装教程

无

#### 使用说明

无

## 开发和部署过程
一、项目初始化
1.  仓库拉取遇到的问题
    - 主分支master和源不对应。需要在IDEA中的Terminal按顺序输入：
    - git remote add origin
    - git pull origin master --allow-unrelated-histories
    - git branch --set-upstream-to=origin/master master
    - git push
    - 如此就可以拉去代码了。
2.  仓库拉取远程代码到本地之后，就可以把本地的代码提交了
3.  Jenkins拉去远程仓库的代码，并部署

# Jenkins+Docker
一、自动化部署
Jenkins构建项目权限报错
> Caused by: com.spotify.docker.client.shaded.javax.ws.rs.ProcessingException: java.io.IOException: Permission denied
执行一下下面命令就可以了
>chmod 777 /var/run/docker.sock
 
